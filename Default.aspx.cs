using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // Response.Write("آمار سایت");

        Label1.Text = Application["Sessions"].ToString();

        if (Request.Browser.Cookies)
        {
            if (Request.Cookies["lastvisit"] == null)
            {
                HttpCookie mycook = new HttpCookie("lastvisit", DateTime.Now.ToString());
                mycook.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(mycook);

            }
            else
            {
                HttpCookie mycook = Request.Cookies["lastvisit"];
                Label12.Text = mycook.Value + ":آخرین مراجعه شما در زمان مقابل بوده است";
                Response.Cookies["lastvisit"].Value = DateTime.Now.ToString();
                Response.Cookies["lastvisit"].Expires = DateTime.Now.AddDays(1);
            }
        }
        else
        {
            
            Label12.Text = "پویشگر شما توانایی پذیرش کوکی را ندارد.";
        }
    }
}