<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
       Application["Hits"] = 0;
        Application["Sessions"] = 0;
        Application["terminated Session"] = 0;

    }

    void Application_BeginRequest(object sender, EventArgs e)
    {
        Application.Lock();
        Application["Hits"] = (int)Application["Hits"] + 1;
        Application.UnLock();
    }


    void Application_End(object sender, EventArgs e)
    {

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        Application.Lock();
        Application["Sessions"] = (int)Application["Sessions"] + 1;
        Application.UnLock();

    }

    void Session_End(object sender, EventArgs e)
    {
        Application.Lock();
        Application["Sessions"] = (int)Application["Sessions"] - 1;
        Application.UnLock();


    }
       
</script>
